Denne resttjeneren er laget i forbindelse med intervju i Husbanken av Philip Emiliam Ditlevsen.

Noe av ubrukt kode er kommentert ut, men ikke fjernet for å vise framgangsmåte underveis. 

Prosjektet kan kjøres i seg selv med å kjøre DemoApplication.java (testet på Windows 10)
Det er bygd på:
    JDK 17 
    Maven 3.8.5
    SpringBoot 2.6.7 (inkluderer også springWeb)


