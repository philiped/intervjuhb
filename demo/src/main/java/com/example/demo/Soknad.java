package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;


public class Soknad {
  
    private String soknadsnummer;
    @NotBlank
    private List<Person> lanetakere = new ArrayList<Person>();
    @NotBlank
    private double lanebelop;
    private String behov;
    @NotBlank
    @Min(0)
    private int lopetid;
    @NotBlank
    @Min(0)
    private int avdragsfriperiode;
    @NotBlank
    private String type; 

    /* Status kunne nok heller vært en slags liste med ulike statuser som man kan loope gjennom med 
    kall for å vise søknadens reise i prosessen  */
    private String status;

    public Soknad(String soknadsnummer, List<Person> lanetakere, double lanebelop, String behov, int lopetid, int avdragsfriperiode, String type, String status){
        super();
        this.soknadsnummer = soknadsnummer;
        this.lanetakere = lanetakere;
        this.lanebelop = lanebelop;
        this.behov = behov;
        this.lopetid = lopetid;
        this.avdragsfriperiode = avdragsfriperiode;
        this.type = type;
        this.status = status;
    }

    public String getSoknadsnummer() {
        return soknadsnummer;
    }

    public List<Person> getLanetakere() {
        return lanetakere;
    }

    public double getLanebelop() {
        return lanebelop;
    }

    public String getBehov() {
        return behov;
    }

    public int getLopetid() {
        return lopetid;
    }

    public int getAvdragsfriperiode() {
        return avdragsfriperiode;
    }

    public String getType() {
        return type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setSoknadsnummer(String soknadsnummer) {
        this.soknadsnummer = soknadsnummer;
    }

}
