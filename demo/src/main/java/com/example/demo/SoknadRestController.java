package com.example.demo;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SoknadRestController {

    @Autowired
    SoknadsService soknadsService;

    /*
    API's are found in http://localhost:8080/swagger-ui/index.html#/

    This should be an unused/removed API. The endpoint would at least need to be protected or only served on internal servers 
    The API returns the soknad of everyone and this would be critical in case users would be able to fetch all soknader */
    @GetMapping("/soknad")
    public List<Soknad> getAllSoknad(){
        return soknadsService.getAllSoknad();
    } 


    /* Gets soknad based on soknadsnummer
    Returns 404 Not Found if there are no Soknad for the soknadsnummer
    Returns 302 Found if there is a Soknad for the soknadsnummer, it also returns the status for said Soknad */
    @GetMapping("/soknad/{soknadsnummer}")
    public ResponseEntity<String> getSoknadBySokNummer(@PathVariable("soknadsnummer") String soknadsNummer){

      String status = soknadsService.getSoknadById(soknadsNummer).getStatus();

      if(status == null | soknadsNummer.length() < 5){
        return new ResponseEntity<String>("Ukjent: søknadsnummeret eksisterer dessverre ikke", HttpStatus.NOT_FOUND);
      }
      
      return new ResponseEntity<String>("Status for søknad: " + soknadsService.getSoknadById(soknadsNummer).getSoknadsnummer() 
                                        + " er " + status,HttpStatus.FOUND);
    }


    /* Posts a soknad based on a JSON body/payload
    Returns 400 BAD_REQUEST if there is invalid data
    Returns 201 CREATED if the Soknad was created correctly */
    @PostMapping("/soknad/add")
    public ResponseEntity<String> addSoknad(@RequestBody Soknad newSoknad) throws Exception {

      String status = soknadsService.addSoknad(newSoknad).getSoknadsnummer();

      if(status == null){
        return new ResponseEntity<String>(status,HttpStatus.BAD_REQUEST);
      }

      System.out.println("Lånesøknad: " + newSoknad.getSoknadsnummer() + newSoknad.getLanetakere() + newSoknad.getLanebelop() + 
      newSoknad.getBehov() + newSoknad.getLopetid() + newSoknad.getAvdragsfriperiode() + newSoknad.getType());

      return new ResponseEntity<String>("Din søknad med søknadsnummer: " + status + " er lagret!", HttpStatus.CREATED);

    }

}