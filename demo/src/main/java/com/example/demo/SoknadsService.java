package com.example.demo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import org.springframework.stereotype.Service;

@Service
public class SoknadsService {

    HashMap<String, Soknad> allSoknad = new HashMap<String, Soknad>();


    public List<Soknad> getAllSoknad() {

        List<Person> PersonLi1 = new ArrayList<Person>();
        List<Person> PersonLi2 = new ArrayList<Person>();

        List<Soknad> soknadMap = new ArrayList<Soknad>();
        
        Person kari = new Person("83728574628", "kari");
        Person ola = new Person("35558574628", "Ola");

        PersonLi1.add(kari);
        PersonLi1.add(ola);
        PersonLi2.add(ola);

        Soknad sok1 = new Soknad("111111", PersonLi1, 234728.23, "bil", 12, 6, "forbruk", "Mottat" );
        Soknad sok2 = new Soknad("222222", PersonLi2, 5692859.44, "hus", 24, 12, "annuitet", "Mottat");

        allSoknad.put(sok1.getSoknadsnummer(), sok1);
        allSoknad.put(sok2.getSoknadsnummer(), sok2);


        for (Soknad i : allSoknad.values()) {
            soknadMap.add(i);
          }

        return soknadMap;


        /*
        allSoknad.add(new Soknad(1, ex3, 3.44, "virus", 12, 6, "Assembly" ));
        allSoknad.add(new Soknad(2, ex3, 5.44, "yepperas", 24, 12, "annuit" ));
                return allSoknad;
        */

    }
    
    public Soknad getSoknadById(String nummer){
        /* Predicate<Soknad> byId = s -> s.getSoknadsnummer() == (nummer); */
        return allSoknad.get(nummer);
    }

    /*
    private Soknad filterSok(Predicate<Soknad> strat) {
        return getAllSoknad().stream().filter(strat).findFirst().orElse(null);
    } */

    public void sokValidate(Soknad valSok) throws Exception{
        for(Person p : valSok.getLanetakere()) {
            if(!(p.getFnr().length() == 11)){
                throw new NumberFormatException("Fødselsnummer er ikke korrekt!");
            }
        }

        if(valSok.getLanebelop() < 0 | valSok.getLopetid() < 0 | valSok.getAvdragsfriperiode() < 0){
            throw new NumberFormatException("Du kan ikke ha negative tall i din lånesøknad!");
        }

    }

    /* "Soknadsnummer" is defined by part of the fnr of a person and a random number, this has security concerns, 
    but works as a makeshift solution for the task.  */
    public Soknad addSoknad(Soknad newSoknad) throws Exception{

        sokValidate(newSoknad);
        String keyString = "";

        for(Person p : newSoknad.getLanetakere()) {
            keyString += p.getFnr().substring(6,8);
            int randomNum = ThreadLocalRandom.current().nextInt(1, 9999);
            keyString += randomNum;
            keyString += p.getFnr().substring(8,10);
        }

        if(allSoknad.get(keyString) != null){
            keyString += ThreadLocalRandom.current().nextInt(1, 9999);
        }

        newSoknad.setSoknadsnummer(keyString);
        newSoknad.setStatus("Mottat");
        allSoknad.put(newSoknad.getSoknadsnummer(), newSoknad);

        return newSoknad;
    }
}
