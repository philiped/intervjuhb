package com.example.demo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class Person {

    /* fnr må være String da det kan begynne på 0*/
    @NotBlank
    @Size(min = 11, max = 11)
    private String fnr;
    @NotBlank
    private String navn;

    public Person(String fnr,  String navn){
        super();
        this.fnr = fnr;
        this.navn = navn;
    }

    public String getFnr() {
        return fnr;
    }

    public String getNavn() {
        return navn;
    }


}
